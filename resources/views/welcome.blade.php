@include('include.header')
@include('include.head')
<div class="container-fluid">
    <div class="row">
        @include('include.sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Please Select</h1>
        </div>
    </div>
</div>
@include('include.footer')