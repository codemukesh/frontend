@include('include.header')
@include('include.head')
<div class="container-fluid">
    <div class="row">
        @include('include.sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Delete Product</h1>

            <form class="form-horizontal" action="" id="deleteProductForm" autocomplete="off" method="post"
                  novalidate="novalidate">
                <fieldset>
                    <!-- Form Name -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="year">Select Shop</label>

                        <div class="col-md-4">
                            <select name='shop_id' class="form-control shopId">
                                @foreach($all_shop as $shop_id => $shop_name)
                                <option value="{{$shop_id}}">{{$shop_name}}</option>
                                @endForeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="year">Select Product</label>

                        <div class="col-md-4">
                            <select name='product_id' class="form-control productId">

                            </select>
                        </div>
                    </div>


                    <!-- Button (Double) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="button1id"></label>

                        <div class="col-md-8">
                            <button class="btn btn-success">Delete</button>

                        </div>
                    </div>

                </fieldset>
            </form>
            <h2 class="sub-header show_result"></h2>

        </div>
    </div>
</div>
@include('include.footer')


<script>
    $(document).ready(function () {
        AddProductSubmit();
    });

    function AddProductSubmit() {

        $("#deleteProductForm").validate({
            // Specify the validation rules
            rules: {
                shop_id: { required: true },
                product_id: { required: true }
            },
            // Specify the validation error messages
            messages: {
                shop_id: { required: "Please Select Shop" },
                product_id: { required: "Please Select Product" }
            },
            submitHandler: function (form) {
                event.preventDefault();
                // loader run
                $(".__loader").removeClass("__hide");
                var shop_id = $('.shopId').val();
                var product_id = $('.productId').val();

                $.ajax({
                    type: "Delete",
                    url: backend_url + "shop/" + shop_id + '/product/' + product_id,
                    cache: false,
                    success: function (html) {
                        $(".__loader").addClass("__hide");
                        if (html.success) {
                            $(".show_result").text(html.message);
                        } else {
                            // error show
                            $(".show_result").text(html.message);
                        }
                    }
                });
            }
        });
    }
</script>