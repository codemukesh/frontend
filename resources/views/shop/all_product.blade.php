@include('include.header')
@include('include.head')
<div class="container-fluid">
    <div class="row">
        @include('include.sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>
                <fieldset>
                    <!-- Form Name -->
                    <legend>All Products</legend>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="year">Select Shop</label>

                        <div class="col-md-4">
                            <select class="form-control shopId">
                                @foreach($all_shop as $shop_id => $shop_name)
                                <option value="{{$shop_id}}">{{$shop_name}}</option>
                                @endForeach
                            </select>
                        </div>
                    </div>
                </fieldset>

            <div class="table-responsive">

            </div>

        </div>
    </div>
</div>
@include('include.footer')

<script>

    $(".shopId").change(function () {
        var shop_id = $('.shopId').val();
        $.ajax({
            type: "GET",
            url: backend_url + "shop/" + shop_id+'/products',
            cache: false,
            dataType: "html",
            success: function (html) {
                $(".__loader").addClass("__hide");
                $(".table-responsive").html(html);
            }
        });
    });

</script>