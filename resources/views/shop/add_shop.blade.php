@include('include.header')
@include('include.head')
<div class="container-fluid">
    <div class="row">
        @include('include.sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Add New Shop</h1>

            <form class="form-horizontal" action="" id="shopForm" autocomplete="off" method="post"
                  novalidate="novalidate">
                <fieldset>
                    <!-- Form Name -->
                    <legend></legend>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Shop Name</label>

                        <div class="col-md-4">
                            <input id="textinput" name="name" type="text" placeholder="Shop Name"
                                   class="form-control input-md">
                        </div>
                    </div>
                    <!-- Button (Double) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput"></label>

                        <div class="col-md-8">
                            <button id="button1id" name="button1id" class="btn btn-success">submit</button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <h2 class="sub-header show_result"></h2>

            <?php
            if (isset($all_shop['success'])) {
                ?>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Shop Id</th>
                            <th>Shop Name</th>
                            <th>DB Name</th>
                            <th>Requests</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($all_shop['data'] as $shopOne) { ?>

                            <tr>
                                <td>{{$shopOne['id']}}</td>
                                <td>{{$shopOne['shop_name']}}</td>
                                <td>{{$shopOne['db_name']}}</td>
                                <td>{{$shopOne['requests']}}</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
@include('include.footer')

<script>
    $(document).ready(function () {
        AddShopSubmit();
    });

    function AddShopSubmit() {

        $("#shopForm").validate({
            // Specify the validation rules
            rules: {
                shop_name: {
                    required: true
                }
            },
            // Specify the validation error messages
            messages: {
                shop_name: {
                    required: "Shop name is required"
                }
            },
            submitHandler: function (form) {
                event.preventDefault();
                // loader run
                $(".__loader").removeClass("__hide");
                var myform = $("#shopForm").serialize();
                $.ajax({
                    type: "POST",
                    url: backend_url + "shop",
                    data: myform,
                    cache: false,
                    success: function (html) {
                        $(".__loader").addClass("__hide");
                        if (html.success) {
                            $(".show_result").text('Shop id:-' + html.data.shop_id + ', Shop Name:-' + html.data.shop_name + ', Shop DB:- ' + html.data.db_name);
                        } else {
                            // error show
                            $(".show_result").text(html.message);
                        }
                        return false;
                    }
                });
                return false;
            }
        });
    }
</script>