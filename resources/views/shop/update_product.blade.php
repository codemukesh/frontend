@include('include.header')
@include('include.head')
<div class="container-fluid">
    <div class="row">
        @include('include.sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Update Product</h1>

            <form class="form-horizontal" action="" id="updateProductForm" autocomplete="off" method="post"
                  novalidate="novalidate">
                <fieldset>
                    <!-- Form Name -->
                    <legend></legend>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="year">Select Shop</label>

                        <div class="col-md-4">
                            <select class="form-control shopId">
                                @foreach($all_shop as $shop_id => $shop_name)
                                <option value="{{$shop_id}}">{{$shop_name}}</option>
                                @endForeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="year">Select Product</label>

                        <div class="col-md-4">
                            <select class="form-control productId">

                            </select>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Category</label>

                        <div class="col-md-4">
                            <input id="category" name="category" type="text" placeholder=""
                                   class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Product</label>

                        <div class="col-md-4">
                            <input id="product" name="product" type="text" placeholder=""
                                   class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Original Price</label>

                        <div class="col-md-4">
                            <input id="price" name="price" type="text" placeholder=""
                                   class="form-control input-md">

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Discount</label>

                        <div class="col-md-4">
                            <input id="discount" name="discount" type="text" placeholder=""
                                   class="form-control input-md">

                        </div>
                    </div>

                    <!-- Button (Double) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="button1id"></label>

                        <div class="col-md-8">
                            <button class="btn btn-success">Update</button>

                        </div>
                    </div>

                </fieldset>
            </form>
            <h2 class="sub-header show_result"></h2>

        </div>
    </div>
</div>
@include('include.footer')


<script>
    $(document).ready(function () {
           AddProductSubmit();
    });

    $(".productId").change(function () {
        var shop_id = $('.shopId').val();
        var productId = $('.productId').val();
        $.ajax({
            type: "GET",
            url: backend_url + "shop/" + shop_id + '/product/' + productId,
            cache: false,
            dataType: "json",
            success: function (html) {
                $(".__loader").addClass("__hide");
                if (html.success) {
                    $("#category").val(html.data.category);
                    $("#product").val(html.data.product);
                    $("#price").val(html.data.price);
                    $("#discount").val(html.data.discount);
                } else {
                    // error show
                    $(".show_result").text(html.message);
                }
            }
        });
    });

    function AddProductSubmit() {

        $("#updateProductForm").validate({
            // Specify the validation rules
            rules: {
                shop_id: { required: true },
                product_id: { required: true },
                category: { required: true },
                product: { required: true },
                price: { required: true }

            },
            // Specify the validation error messages
            messages: {
                shop_id: { required: "Please Select Shop" },
                product_id: { required: "Please Select Product" },
                category: { required: "category is required" },
                product: { required: "product is required" },
                price: { required: "price is required" }
            },
            submitHandler: function (form) {
                event.preventDefault();
                // loader run
                $(".__loader").removeClass("__hide");
                var myform = $("#updateProductForm").serialize();
                var shop_id = $('.shopId').val();
                var product_id = $('.productId').val();

                $.ajax({
                    type: "PATCH",
                    url: backend_url + "shop/" + shop_id + '/product/'+product_id,
                    data: myform,
                    cache: false,
                    success: function (html) {
                        $(".__loader").addClass("__hide");
                        if (html.success) {
                            $(".show_result").text(html.message);
                        } else {
                            // error show
                            $(".show_result").text(html.message);
                        }
                    }
                });
            }
        });
    }
</script>