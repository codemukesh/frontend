<div class="push__notification __active __success ">
    <div class="__notification">
        <span class="__icon"><i></i></span>
        <p class="push__notification__msg ">AAAAA</p>
    </div>
</div><!-- end push__notification -->

<div class="__loader __hide">
    <img src="{{ asset('/')}}image/loader.svg" alt=""/>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('/')}}js/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('/')}}js/jquery.validate.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('/')}}js/style.js"></script>
<script src="{{ asset('/')}}js/bootstrap.min.js"></script>
</body>
</html>