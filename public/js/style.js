/**
 * Created by Mukesh Jagga on 3/12/2017.
 */


function remove_error_notification() {
    $(".push__notification").removeClass("__error __active");
    $(".push__notification__msg").text("Oops something went wrong.");
}

function remove_success_notification() {
    $(".push__notification").removeClass("__success __active");
    $(".push__notification__msg").text("Successfully done.");
}

$(".shopId").change(function () {
    var shop_id = $('.shopId').val();
    $.ajax({
        type: "GET",
        url: backend_url + "shop/" + shop_id,
        cache: false,
        dataType: "html",
        success: function (html) {
            $(".__loader").addClass("__hide");
            $(".productId").html(html);
        }
    });
});