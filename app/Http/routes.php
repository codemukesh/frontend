<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/add_shop', 'ShopController@addShop');

Route::get('/shop/add_product', 'ShopController@addProduct');

Route::get('/shop/update_product', 'ShopController@updateProduct');

Route::get('/shop/delete_product', 'ShopController@deleteProduct');

Route::get('/shop/all_products', 'ShopController@allProducts');