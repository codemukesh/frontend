<?php

namespace App\Http\Controllers;

use App\Curl;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShopController extends Controller
{
    public function index()
    {
        return view('shop.index');
    }

    public function addShop()
    {
        $url = env('backend_server').'shop';
        $all_shop = Curl::CURL_GET($url);

        return view('shop.add_shop', array('all_shop'=>$all_shop));
    }

    public function addProduct()
    {
        $url = env('backend_server') . 'shop_name';

        $all_shop = Curl::CURL_POST($url,array());

        return view('shop.add_product', array('all_shop' => $all_shop));
    }

    public function updateProduct()
    {
        $url = env('backend_server') . 'shop_name';
        $all_shop = Curl::CURL_POST($url,array());

        return view('shop.update_product', array('all_shop' => $all_shop));
    }

    public function deleteProduct()
    {
        $url = env('backend_server') . 'shop_name';
        $all_shop = Curl::CURL_POST($url,array());

        return view('shop.delete_product', array('all_shop' => $all_shop));
    }

    public function allProducts()
    {
        $url = env('backend_server') . 'shop_name';
        $all_shop = Curl::CURL_POST($url,array());

        return view('shop.all_product', array('all_shop' => $all_shop));
    }
}
